﻿using System.Linq;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;

namespace _4_NotificationCenter_Agent
{
    public sealed class NotificationsChangedTask : IBackgroundTask
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var toasts = ToastNotificationManager.History.GetHistory();
            if (toasts != null)
            {
                var count = toasts.Count;
                if (count == 0)
                {
                    BadgeUpdateManager.CreateBadgeUpdaterForApplication().Clear();
                }
                else
                {
                    var xmlDoc = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
                    var badgeNode = xmlDoc.SelectSingleNode("//badge");
                    var valueAttribute = badgeNode.Attributes.Single(a => a.LocalName.ToString() == "value");
                    valueAttribute.NodeValue = count.ToString();

                    var notification = new BadgeNotification(xmlDoc);
                    BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(notification);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _4_NotificationCenter_App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Register();
        }

        private async void Register()
        {
            foreach (var t in BackgroundTaskRegistration.AllTasks)
            {
                t.Value.Unregister(true);
            }
            var builder = new BackgroundTaskBuilder
            {
                TaskEntryPoint = "_4_NotificationCenter_Agent.NotificationsChangedTask",
                Name = "ToastTrigger"
            };
            builder.SetTrigger(new ToastNotificationHistoryChangedTrigger());

            BackgroundExecutionManager.RemoveAccess();
            var status = await BackgroundExecutionManager.RequestAccessAsync();
            var registration = builder.Register();
        }

        private void OnPopNotification(object sender, RoutedEventArgs e)
        {
            var xml = @"
<toast>
  <visual>
    <binding template=""ToastGeneric"">
      <text>Toast</text>
      <text>Just another notification</text>
    </binding>
  </visual>
</toast>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var toast = new ToastNotification(xmlDoc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);

            UpdateBadge();
        }

        private void UpdateBadge()
        {
            var toasts = ToastNotificationManager.History.GetHistory();
            if (toasts != null)
            {
                var count = toasts.Count;
                if (count == 0)
                {
                    BadgeUpdateManager.CreateBadgeUpdaterForApplication().Clear();
                }
                else
                {
                    var xmlDoc = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
                    var badgeNode = xmlDoc.SelectSingleNode("//badge");
                    var valueAttribute = badgeNode.Attributes.Single(a => a.LocalName.ToString() == "value");
                    valueAttribute.NodeValue = count.ToString();

                    var notification = new BadgeNotification(xmlDoc);
                    BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(notification);
                }
            }

        }
    }
}

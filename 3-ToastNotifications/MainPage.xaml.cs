﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _3_ToastNotifications
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnPopNonInteractive(object sender, RoutedEventArgs e)
        {
            var xml = @"
<toast>
  <visual>
    <binding template=""ToastGeneric"">
      <text>Non-interactive</text>
      <text>This is a non-interactive toast message</text>
    </binding>
  </visual>
</toast>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            
            var toast = new ToastNotification(xmlDoc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void OnPopNavigateToUrl(object sender, RoutedEventArgs e)
        {
            var xml = @"
<toast>
 <visual>
   <binding template=""ToastGeneric"">
     <text>Damir's Corner</text>
     <text>Check this great website.</text>
   </binding>
 </visual>
 <actions>
   <action activationType=""protocol"" arguments=""http://www.damirscorner.com"" content=""Open Site""/>
 </actions>
</toast>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var toast = new ToastNotification(xmlDoc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void OnPopInteractive(object sender, RoutedEventArgs e)
        {
            var xml = @"
<toast>
 <visual>
   <binding template=""ToastGeneric"">
     <text>Interactive</text>
     <text>Startup arguments:</text>
   </binding>
 </visual>
 <actions>
   <input id=""startupArgs"" type=""text""/>
   <action activationType=""foreground"" arguments=""toast"" content=""Launch""/>
 </actions>
</toast>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var toast = new ToastNotification(xmlDoc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            InputText.Text = e.Parameter?.ToString() ?? "";
            base.OnNavigatedTo(e);
        }

        private void OnPopSystemReminder(object sender, RoutedEventArgs e)
        {
            var xml = @"<toast scenario=""reminder"">
  <visual>
    <binding template=""ToastGeneric"">
      <text>Komunikacija med aplikacijami</text>
      <text>Piran</text>
      <text>13:45 - 14:45</text>
    </binding>
  </visual>
  <actions>
    <input id=""snoozeTime"" type=""selection"" defaultInput=""15"">
      <selection id=""1"" content=""1 minute""/>
      <selection id=""15"" content=""15 minutes""/>
      <selection id=""60"" content=""1 hour""/>
      <selection id=""240"" content=""4 hours""/>
      <selection id=""1440"" content=""1 day""/>
    </input>
    <action
      activationType=""system""
      arguments=""snooze""
      hint-inputId=""snoozeTime""
      content=""""/>
    <action
      activationType=""system""
      arguments=""dismiss""
      content=""""/>
  </actions>
</toast>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var toast = new ToastNotification(xmlDoc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}

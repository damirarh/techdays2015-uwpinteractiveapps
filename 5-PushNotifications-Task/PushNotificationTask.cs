﻿using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Networking.PushNotifications;
using Windows.UI.Notifications;

namespace _5_PushNotifications_Task
{
    public sealed class PushNotificationTask : IBackgroundTask
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var body = ((RawNotification) taskInstance.TriggerDetails).Content;
            var xml = $@"
<toast>
  <visual>
    <binding template=""ToastGeneric"">
      <text>Toast from Task</text>
      <text>{body}</text>
    </binding>
  </visual>
</toast>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var toast = new ToastNotification(xmlDoc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
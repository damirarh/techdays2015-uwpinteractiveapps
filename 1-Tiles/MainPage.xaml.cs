﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _1_Tiles
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private int _badgeValue = 1;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnRefreshTile(object sender, RoutedEventArgs e)
        {
            //<tile>
            //	<visual version="4">
            //		<binding fallback="TileSquarePeekImageAndText04" template="TileSquare150x150PeekImageAndText04">
            //			<image id="1" src=""/>
            //			<text id="1"/>
            //		</binding>
            //	</visual>
            //</tile>
            var xmlDoc = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150PeekImageAndText04);
            var imageNode = xmlDoc.SelectSingleNode("//image[@id='1']");
            var srcAttribute = imageNode.Attributes.Single(a => a.LocalName.ToString() == "src");
            srcAttribute.NodeValue = "ms-appx:///Assets/StoreLogo.png";
            var textNode = xmlDoc.SelectSingleNode("//text[@id='1']");
            textNode.InnerText = $"Refreshed at {DateTime.Now}";

            var notification = new TileNotification(xmlDoc);
            TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);
        }

        private void OnUpdateBadge(object sender, RoutedEventArgs e)
        {
            //<badge value=""/>
            var xmlDoc = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
            var badgeNode = xmlDoc.SelectSingleNode("//badge");
            var valueAttribute = badgeNode.Attributes.Single(a => a.LocalName.ToString() == "value");
            valueAttribute.NodeValue = _badgeValue.ToString();

            var notification = new BadgeNotification(xmlDoc);
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(notification);
            _badgeValue++;
        }

        private async void OnCreateSecondaryTile(object sender, RoutedEventArgs e)
        {
            var uniqueId = "Id" + SecondaryTileId.Text;
            if (!SecondaryTile.Exists(uniqueId))
            {
                var tile = new SecondaryTile(uniqueId, $"Secondary Tile \"{uniqueId}\"", uniqueId, new Uri("ms-appx:///Assets/StoreLogo.png"), TileSize.Default);
                await tile.RequestCreateAsync();
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SecondaryTileId.Text = e.Parameter?.ToString() ?? "";
            base.OnNavigatedTo(e);
        }
    }
}

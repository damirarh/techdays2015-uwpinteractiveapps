﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using NotificationsExtensions.Tiles;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _2_AdaptiveTemplates
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly string _dayShort = "Tue";
        private readonly string _dayLong = "Tuesday";
        private readonly string _dayInMonth = "22";
        private readonly string _title1 = "Interaktivne UWA aplikacije";
        private readonly string _room1 = "Piran";
        private readonly string _timeSlot1 = "Today: 12:30 - 13:30";
        private readonly string _timeSlot1Short = "Tue: 12:30";
        private readonly string _title2 = "Komunikacija med aplikacijami";
        private readonly string _room2 = "Piran";
        private readonly string _timeSlot2 = "Today: 13:45 - 14:45";

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnPinFromXml(object sender, RoutedEventArgs e)
        {
            var content = $@"
<tile>
  <visual>

    <binding template=""TileSmall"" hint-textStacking=""center"">
      <text hint-align=""center"">{_dayShort}</text>
      <text hint-align=""center"" hint-style=""body"">{_dayInMonth}</text>
    </binding>

    <binding template=""TileMedium"" branding=""name"" displayName=""{_dayLong} {_dayInMonth}"">
      <text hint-wrap=""true"" hint-maxLines=""2"">{_title1}</text>
      <text hint-style=""captionsubtle"">{_timeSlot1Short}</text>
    </binding>

    <binding template=""TileWide"" branding=""nameAndLogo"" displayName=""{_dayLong} {_dayInMonth}"">
      <text>{_title1}</text>
      <text hint-style=""captionsubtle"">{_room1}</text>
      <text hint-style=""captionsubtle"">{_timeSlot1}</text>
    </binding>

    <binding template=""TileLarge"" branding=""nameAndLogo"" displayName=""{_dayLong} {_dayInMonth}"">
      <group>
        <subgroup>
          <text hint-wrap=""true"">{_title1}</text>
          <text hint-style=""captionsubtle"">{_room1}</text>
          <text hint-style=""captionsubtle"">{_timeSlot1}</text>
        </subgroup>
      </group>
      
      <text />

      <group>
        <subgroup>
          <text hint-wrap=""true"">{_title2}</text>
          <text hint-style=""captionsubtle"">{_room2}</text>
          <text hint-style=""captionsubtle"">{_timeSlot2}</text>
        </subgroup>
      </group>
    </binding>

  </visual>
</tile>";

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(content);

            if (!SecondaryTile.Exists("FromXml"))
            {
                var tile = new SecondaryTile("FromXml", "FromXml", "FromXml", new Uri("ms-appx:///Assets/StoreLogo.png"), TileSize.Default);
                tile.VisualElements.Wide310x150Logo = new Uri("ms-appx:///Assets/Wide310x150Logo.scale-200.png");
                tile.VisualElements.Square310x310Logo = new Uri("ms-appx:///Assets/Square150x150Logo.scale-200.png");
                await tile.RequestCreateAsync();
            }

            var notification = new TileNotification(xmlDoc);
            TileUpdateManager.CreateTileUpdaterForSecondaryTile("FromXml").Update(notification);
        }

        private async void OnPinFromCode(object sender, RoutedEventArgs e)
        {
            var content = new TileContent
            {
                Visual = new TileVisual
                {
                    TileSmall = new TileBinding
                    {
                        Content = new TileBindingContentAdaptive
                        {
                            TextStacking = TileTextStacking.Center,
                            Children =
                            {
                                new TileText
                                {
                                    Align = TileTextAlign.Center,
                                    Text = _dayShort
                                },
                                new TileText
                                {
                                    Align = TileTextAlign.Center,
                                    Style = TileTextStyle.Body,
                                    Text = _dayInMonth
                                }
                            }
                        }
                    },
                    TileMedium = new TileBinding
                    {
                        Branding = TileBranding.Name,
                        DisplayName = $"{_dayLong} {_dayInMonth}",
                        Content = new TileBindingContentAdaptive
                        {
                            Children =
                            {
                                new TileText
                                {
                                    Wrap = true,
                                    MaxLines = 2,
                                    Text = _title1
                                },
                                new TileText
                                {
                                    Style = TileTextStyle.CaptionSubtle,
                                    Text = _timeSlot1Short
                                }
                            }
                        }
                    },
                    TileWide = new TileBinding
                    {
                        Branding = TileBranding.NameAndLogo,
                        DisplayName = $"{_dayLong} {_dayInMonth}",
                        Content = new TileBindingContentAdaptive()
                        {
                            Children =
                            {
                                new TileText
                                {
                                    Text = _title1
                                },
                                new TileText
                                {
                                    Style = TileTextStyle.CaptionSubtle,
                                    Text = _room1
                                },
                                new TileText
                                {
                                    Style = TileTextStyle.CaptionSubtle,
                                    Text = _timeSlot1
                                }
                            }
                        }
                    },
                    TileLarge = new TileBinding
                    {
                        Branding = TileBranding.NameAndLogo,
                        DisplayName = $"{_dayLong} {_dayInMonth}",
                        Content = new TileBindingContentAdaptive
                        {
                            Children =
                            {
                                new TileGroup
                                {
                                    Children =
                                    {
                                        new TileSubgroup
                                        {
                                            Children =
                                            {
                                                new TileText
                                                {
                                                    Wrap = true,
                                                    Text = _title1
                                                },
                                                new TileText
                                                {
                                                    Style = TileTextStyle.CaptionSubtle,
                                                    Text = _room1
                                                },
                                                new TileText
                                                {
                                                    Style = TileTextStyle.CaptionSubtle,
                                                    Text = _timeSlot1
                                                }
                                            }
                                        }
                                    }
                                },
                                new TileText(),
                                new TileGroup
                                {
                                    Children =
                                    {
                                        new TileSubgroup
                                        {
                                            Children =
                                            {
                                                new TileText
                                                {
                                                    Wrap = true,
                                                    Text = _title2
                                                },
                                                new TileText
                                                {
                                                    Style = TileTextStyle.CaptionSubtle,
                                                    Text = _room2
                                                },
                                                new TileText
                                                {
                                                    Style = TileTextStyle.CaptionSubtle,
                                                    Text = _timeSlot2
                                                }
                                            }
                                        }
                                    }
                                },
                            }
                        }
                    }
                }
            };

            if (!SecondaryTile.Exists("FromCode"))
            {
                var tile = new SecondaryTile("FromCode", "FromCode", "FromCode", new Uri("ms-appx:///Assets/StoreLogo.png"), TileSize.Default);
                tile.VisualElements.Wide310x150Logo = new Uri("ms-appx:///Assets/Wide310x150Logo.scale-200.png");
                tile.VisualElements.Square310x310Logo = new Uri("ms-appx:///Assets/Square150x150Logo.scale-200.png");
                await tile.RequestCreateAsync();
            }

            var notification = new TileNotification(content.GetXml());
            TileUpdateManager.CreateTileUpdaterForSecondaryTile("FromCode").Update(notification);
        }
    }
}
